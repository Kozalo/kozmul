import sublime, sublime_plugin

class KozmulBoldTextCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		for sel in self.view.sel():
			self.view.insert(edit, sel.begin(), "**")
			self.view.insert(edit, sel.end()+2, "**")

class KozmulItalicTextCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		for sel in self.view.sel():
			self.view.insert(edit, sel.begin(), "__")
			self.view.insert(edit, sel.end()+2, "__")

class KozmulUnorderedListCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		corrector = 0

		for sel in self.view.sel():
			lines = self.view.lines( sublime.Region(sel.begin(), sel.end()) )
			lastLine = len(lines) - 1

			for i, line in enumerate(lines):
				self.view.insert(edit, line.begin() + corrector, "+ ")

				corrector = corrector + 2
				if i == lastLine:
					self.view.insert(edit, line.end() + corrector, "\n+")				

class KozmulOrderedListCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		corrector = 0

		for sel in self.view.sel():
			lines = self.view.lines( sublime.Region(sel.begin(), sel.end()) )
			lastLine = len(lines) - 1

			for i, line in enumerate(lines):
				self.view.insert(edit, line.begin() + corrector, "# ")

				corrector = corrector + 2
				if i == lastLine:
					self.view.insert(edit, line.end() + corrector, "\n#")	
