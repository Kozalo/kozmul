document.addEventListener("DOMContentLoaded", function(e) {
    let processor = new KozMUL({
        transformHeadings: true,
        transformLinks: true,
        transformImages: true,
        transformTables: true,
        transformCodeTags: true,
        transformSubstitutions: true,
        transformHorizontalLines: true
    }, {
        targetBlank: true,
        tableClasses: []
    });

    document.getElementById("content").innerHTML = processor.toHTML(document.getElementById("source").value);
});
