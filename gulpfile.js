var gulp = require('gulp');
var uglifyjs = require('uglify-js-harmony');
var minifier = require('gulp-uglify/minifier');
var cssmin = require('gulp-cssmin');
var htmlmin = require('gulp-htmlmin');
var gutil = require('gulp-util');
var exec = require('child_process').exec;


gulp.task('default', function() {
    console.log("You can type 'gulp <taskname>' to use automatic build system.");
    console.log("There are some following tasks:");
    console.log("* build -- Minifies the script file and creates a build in the /dist directory.");
    console.log("* build-docs -- Runs JsDoc to make documentation.");
    console.log("* build-manual -- Minifies the manual.");
    console.log("* build-editor -- Minifies the editor.");
    console.log("* watch-docs -- Begins to watch for modified files in the /src directory and for the docs/jsdoc-readme.md file. If any file was changed, rebuild the documentation.");
});


gulp.task('build', function() {
    gulp.src('./src/*.js')
        .pipe(minifier({compress: false}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest('./dist'));
});


gulp.task('build-docs', function() {
    exec('jsdoc -c ./docs/jsdoc-conf.json -R ./docs/jsdoc-readme.md', (err, stdout, stderr) => {
        console.log(stderr);
    });
});

gulp.task('watch-docs', function() {
    gulp.watch(['./src/*.js', './docs/jsdoc-readme.md'], ['build-docs']);
});


gulp.task('build-manual', function() {
    var pathBase = './docs/manual_viewer/';

    gulp.src(pathBase + '*.js')
        .pipe(minifier({}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest(pathBase + 'dist'));

    gulp.src(pathBase + '*.css')
        .pipe(cssmin()).on('error', gutil.log)
        .pipe(gulp.dest(pathBase + 'dist'));

    gulp.src(pathBase + '*.html')
        .pipe(gulp.dest(pathBase + 'dist'));
});


gulp.task('build-editor', function() {
    var pathBase = './editor/';

    gulp.src(pathBase + '*.js')
        .pipe(minifier({}, uglifyjs)).on('error', gutil.log)
        .pipe(gulp.dest(pathBase + 'dist'));

    gulp.src(pathBase + '*.css')
        .pipe(cssmin()).on('error', gutil.log)
        .pipe(gulp.dest(pathBase + 'dist'));

    gulp.src(pathBase + '*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(pathBase + 'dist'));
});
