var processor, targetBlankSwitch, indentationSyntaxSwitch;

function updateTextProcessor() {
    processor = new KozMUL({
        transformHeadings: true,
        transformLinks: true,
        transformImages: true,
        transformTables: true,
        transformCodeTags: true,
        transformSubstitutions: true,
        transformHorizontalLines: true
    }, {
        targetBlank: targetBlankSwitch.checked,
        tableClasses: [],
        listsWithIndentations: indentationSyntaxSwitch.checked
    });

    processText();
}

function processText() {
    document.getElementById("result").innerHTML = processor.toHTML(document.getElementById("editor").value);
}


document.addEventListener("DOMContentLoaded", function(e) {
    targetBlankSwitch = document.getElementById("targetBlankSwitch");
    indentationSyntaxSwitch = document.getElementById("indentationSyntaxSwitch");

    updateTextProcessor();

    document.getElementById("editor").addEventListener("input", processText);
    targetBlankSwitch.addEventListener("change", updateTextProcessor);
    indentationSyntaxSwitch.addEventListener("change", updateTextProcessor);
});
