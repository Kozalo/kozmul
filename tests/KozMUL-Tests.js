"use strict";

describe("KozMUL", function() {
    let processor = new KozMUL({
        transformHTMLEntities: true,
        transformBasicStyleTags: true,
        transformTypographicalCharacters: true,
        transformSubscriptAndSuperscriptTags: true,
        transformParagraphs: true,
        transformLists: true,
        transformQuotations: true,
        transformHorizontalLines: true,
        transformHeadings: true,
        transformLinks: true,
        transformImages: true,
        transformTables: true,
        transformCodeTags: true,
        transformSubstitutions: true
    }, {
        listsWithIndentations: false
    });


    describe("Tests for toSingleLine()", function() {
        it("Three lines", function () {
            let text = "The first line;\nThe second one;\nThe third.";
            let result = processor._toSingleLine(text);
            expect(result).toBe("The first line;[[eol]]The second one;[[eol]]The third.");
        });
    });

    describe("Tests for basicTransformations()", function() {
        it("EOL test", function() {
            let text = "Hello! Here is[[eol]]a line break!";
            let result = processor._basicTransformations(text);
            expect(result).toBe("Hello! Here is<br/>a line break!");
        });

        it("Eliminate double spaces", function() {
            let text = "Hello,  world! Oops! I've made  a typo!";
            let result = processor._basicTransformations(text);
            expect(result).toBe("Hello, world! Oops! I've made a typo!");
        });
    });


    describe("Tests for removeEscapingCharacters()", function() {
        it("Just escaping", function() {
            let text = "Hello! Here is a *\\\\*line*\\\\*!";
            let result = processor._removeEscapingCharacters(text);
            expect(result).toBe("Hello! Here is a **line**!");
        });
    });


    describe("Tests for transformHTMLEntities()", function() {
        it("Eliminate HTML tags", function () {
            let text = "<script>alert('XSS!')</script>";
            let result = processor._transformHTMLEntities(text);
            expect(result).toBe("&lt;script&gt;alert(&apos;XSS!&apos;)&lt;/script&gt;");
        });

        it("Escaping", function () {
            let text = "\"Kozalo & Chocola's Wedding\"";
            let result = processor._transformHTMLEntities(text);
            expect(result).toBe("&quot;Kozalo &amp; Chocola&apos;s Wedding&quot;");
        });
    });


    describe("Tests for transformTypographicalCharacters()", function() {
        it("Double hyphens to a dash", function () {
            let text = "There's no need to lock the door -- I'm coming out after you.";
            let result = processor._transformTypographicalCharacters(processor._transformHTMLEntities(text));
            expect(result).toBe("There&apos;s no need to lock the door &mdash; I&apos;m coming out after you.");
        });

        it("Double angle brackets to yolochki", function () {
            let text = "<<Curiosity>> is a car-sized robotic rover.";
            let result = processor._transformTypographicalCharacters(processor._transformHTMLEntities(text));
            expect(result).toBe("&laquo;Curiosity&raquo; is a car-sized robotic rover.");
        });

        it("C, R and TM", function() {
            let text1 = "Some(r) text(tm) by (c) me.";
            let text2 = "Some(R) text(TM) by (C) me.";
            let result1 = processor._transformTypographicalCharacters(text1);
            let result2 = processor._transformTypographicalCharacters(text2);
            let expectingText = "Some&reg; text&trade; by &copy; me.";
            expect(result1).toBe(expectingText);
            expect(result2).toBe(expectingText);
        });
    });


    describe("Tests transformHorizontalLines()", function() {
        it("Six hyphens", function() {
            let text = "------";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("------");
        });

        it("Six hyphens and line breaks around", function() {
            let text = "[[eol]]------[[eol]]";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("<hr/>");
        });

        it("Three dashes and line breaks", function() {
            let text = "[[eol]]———[[eol]]";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("<hr/>");
        });

        it("Three dashes as HTML entities and line breaks", function() {
            let text = "[[eol]]&mdash;&mdash;&mdash;[[eol]]";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("<hr/>");
        });

        it("Five hyphens and line breaks", function() {
            let text = "[[eol]]-----[[eol]]";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("[[eol]]-----[[eol]]");
        });

        it("Six dashes and line breaks", function() {
            let text = "[[eol]]——————[[eol]]";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("<hr/>");
        });

        it("Test string", function() {
            let text = "First line.[[eol]]------[[eol]]Last line.";
            let result = processor._transformHorizontalLines(text);
            expect(result).toBe("First line.<hr/>Last line.");
        });
    });


    describe("Tests for transformBasicStyleTags()", function() {
        it("Bold text", function () {
            let text = "Hello! **Here** is some **bold text**";
            let result = processor._transformBasicStyleTags(text);
            expect(result).toBe("Hello! <strong>Here</strong> is some <strong>bold text</strong>");
        });

        it("Italic text", function () {
            let text = "Hello! __Here__ is some __italic text__";
            let result = processor._transformBasicStyleTags(text);
            expect(result).toBe("Hello! <em>Here</em> is some <em>italic text</em>");
        });

        it("Strikethrough text", function () {
            let text = "Hello! ~~Here~~ is some ~~strikethrough text~~";
            let result = processor._transformBasicStyleTags(text);
            expect(result).toBe("Hello! <s>Here</s> is some <s>strikethrough text</s>");
        });
    });


    describe("Tests for transformHeadings()", function() {
        it("Headings", function () {
            let text = "== Heading 1[[eol]]Usual text.[[eol]]$$ Heading 2[[eol]]Usual text again.[[eol]]%% Heading 3[[eol]]The last piece of text.";
            let result = processor._transformHeadings(text);
            expect(result).toBe("<h3>Heading 1</h3>Usual text.[[eol]]<h4>Heading 2</h4>Usual text again.[[eol]]<h5>Heading 3</h5>The last piece of text.");
        });
    });


    describe("Tests for transformParagraphs()", function() {
        let paragraphsAndHeadingsOnlyProcessor = new KozMUL({
            transformHTMLEntities: false,
            transformBasicStyleTags: false,
            transformTypographicalCharacters: false,
            transformSubscriptAndSuperscriptTags: false,
            transformParagraphs: true,
            transformLists: false,
            transformQuotations: false,
            transformHorizontalLines: false,
            transformHeadings: true,
            transformLinks: false,
            transformImages: false,
            transformTables: false,
            transformCodeTags: false,
            transformSubstitutions: false
    });

        it("Paragraphs", function () {
            let text = "First paragraph.[[eol]][[eol]]Second paragraph.[[eol]][[eol]]Third paragraph.";
            let result = paragraphsAndHeadingsOnlyProcessor.toHTML(text);
            expect(result).toBe("<p>First paragraph.</p><p>Second paragraph.</p><p>Third paragraph.</p>");
        });

        it("Paragraphs and EOLs", function () {
            let text = "First paragraph. First line.[[eol]]Second line.[[eol]]Third.[[eol]][[eol]]Second paragraph. First line.[[eol]]Second.[[eol]][[eol]]Third paragraph.";
            let result = paragraphsAndHeadingsOnlyProcessor.toHTML(text);
            expect(result).toBe("<p>First paragraph. First line.<br/>Second line.<br/>Third.</p><p>Second paragraph. First line.<br/>Second.</p><p>Third paragraph.</p>");
        });

        it("Paragraphs, headings and EOLs", function () {
            let text = "== Heading 1[[eol]][[eol]]Usual text.[[eol]][[eol]]$$ Subject 1[[eol]][[eol]]Usual text again.[[eol]][[eol]]== Heading 2 ==[[eol]][[eol]]$$ Subject 2 $$[[eol]][[eol]]The last usual text.";
            let result = paragraphsAndHeadingsOnlyProcessor.toHTML(text);
            expect(result).toBe("<h3>Heading 1</h3><p>Usual text.</p><h4>Subject 1</h4><p>Usual text again.</p><h3>Heading 2</h3><br/><h4>Subject 2</h4><p>The last usual text.</p>");
        });
    });


    describe("Tests for transformSubscriptAndSuperscriptTags()", function() {
        it("Exponent and index", function () {
            let text = "H.{2}O. 3^{2}=9.";
            let result = processor._transformSubscriptAndSuperscriptTags(text);
            expect(result).toBe("H<sub>2</sub>O. 3<sup>2</sup>=9.");
        });
    });


    describe("Tests for transformCodeTags()", function() {
        it("Code formatting", function() {
            let text = "To append a value to the end of an array use the ``Array.prototype.push()`` method.";
            let result = processor._transformCodeTags(text);
            expect(result).toBe("To append a value to the end of an array use the <code>Array.prototype.push()</code> method.");
        });

        it("More real example", function() {
            let text = "To append a value to the end of an array use the ``Array.prototype.push()`` method:[[eol]]```if (true) {[[eol]]    // do something...[[eol]]}```";
            let result = processor._basicTransformations(processor._transformCodeTags(text));
            expect(result).toBe("To append a value to the end of an array use the <code>Array.prototype.push()</code> method:<br/><pre><code>if (true) {<br/>    // do something...<br/>}</code></pre>");
        });
    });


    describe("Tests for transformLinks()", function() {
        let linkProcessor = new KozMUL({
            transformHTMLEntities: false,
            transformBasicStyleTags: false,
            transformTypographicalCharacters: false,
            transformSubscriptAndSuperscriptTags: false,
            transformParagraphs: false,
            transformLists: false,
            transformQuotations: false,
            transformHorizontalLines: false,
            transformHeadings: false,
            transformLinks: true,
            transformImages: true,
            transformTables: false,
            transformCodeTags: false,
            transformSubstitutions: false
        }, {
            targetBlank: false
        });

        it("Link", function() {
            let text = "See the following website: @[[http://kozalo.ru]].";
            let result = linkProcessor.toHTML(text);
            expect(result).toBe('See the following website: <a href="http://kozalo.ru">http://kozalo.ru</a>.');
        });

        it("Link with label", function() {
            let text = "Here is @[a link][http://kozalo.ru].";
            let result = linkProcessor.toHTML(text);
            expect(result).toBe('Here is <a href="http://kozalo.ru">a link</a>.');
        });

        it("Some links", function() {
            let text = "@[[http://kozalo.ru/congrats]] was developed by @[Kozalo][http://vk.com/kozalo] (@[[http://kozalo.ru]]).";
            let result = linkProcessor.toHTML(text);
            expect(result).toBe('<a href="http://kozalo.ru/congrats">http://kozalo.ru/congrats</a> was developed by <a href="http://vk.com/kozalo">Kozalo</a> (<a href="http://kozalo.ru">http://kozalo.ru</a>).');
        });

        it("targetBlank is enabled", function() {
            let targetBlankProcessor = new KozMUL({
                transformHTMLEntities: false,
                transformBasicStyleTags: false,
                transformTypographicalCharacters: false,
                transformSubscriptAndSuperscriptTags: false,
                transformParagraphs: false,
                transformLists: false,
                transformQuotations: false,
                transformHorizontalLines: false,
                transformHeadings: false,
                transformLinks: true,
                transformImages: true,
                transformTables: false,
                transformCodeTags: false,
                transformSubstitutions: false
            }, {
                targetBlank: true
            });

            let text = "Let's open @[this link][http://nekochan.ru] in a new tab!";
            let result = targetBlankProcessor.toHTML(text);
            expect(result).toBe('Let\'s open <a href="http://nekochan.ru" target="_blank">this link</a> in a new tab!');
        });

        it("Image link", function() {
            let text = "@![[https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg]]";
            let result = linkProcessor.toHTML(text);
            expect(result).toBe('<a href="https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg"><img src="https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg"></a>');
        });

        it("Image link with alternative text", function() {
            let text = "@![Fox girl][https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg]";
            let result = linkProcessor.toHTML(text);
            expect(result).toBe('<a href="https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg"><img src="https://pp.vk.me/c637722/v637722631/23758/O8eaez7PM3I.jpg" alt="Fox girl"></a>');
        });

        it("Image link processing if images or links are disabled", function () {
            let text = "@![[https://pp.vk.me/c637722/v637722631/23758/O8eaez8PM3I.jpg]]";

            let tmpProcessor = new KozMUL({
                transformHTMLEntities: false,
                transformBasicStyleTags: false,
                transformTypographicalCharacters: false,
                transformSubscriptAndSuperscriptTags: false,
                transformParagraphs: false,
                transformLists: false,
                transformQuotations: false,
                transformHorizontalLines: false,
                transformHeadings: false,
                transformLinks: true,
                transformImages: false,
                transformTables: false,
                transformCodeTags: false,
                transformSubstitutions: false
            }, {
                targetBlank: false
            });
            expect(tmpProcessor.toHTML(text)).toBe(text);

            tmpProcessor._operations.transformImages = true;
            tmpProcessor._operations.transformLinks = false;
            expect(tmpProcessor.toHTML(text)).toBe('@<img src="https://pp.vk.me/c637722/v637722631/23758/O8eaez8PM3I.jpg">');
        });
    });


    describe("Tests for transformImages()", function() {
        let imageProcessor = new KozMUL({
            transformHTMLEntities: false,
            transformBasicStyleTags: false,
            transformTypographicalCharacters: false,
            transformSubscriptAndSuperscriptTags: false,
            transformParagraphs: false,
            transformLists: false,
            transformQuotations: false,
            transformHorizontalLines: false,
            transformHeadings: false,
            transformLinks: true,
            transformImages: true,
            transformTables: false,
            transformCodeTags: false,
            transformSubstitutions: false
        }, {
            targetBlank: false
        });

        it("Image", function() {
            let text = "Image: ![[http://kozalo.ru/images/service/logo.png]]";
            let result = imageProcessor.toHTML(text);
            expect(result).toBe('Image: <img src="http://kozalo.ru/images/service/logo.png">');
        });

        it("Image with 'alt'", function() {
            let text = "Image: ![Logo][http://kozalo.ru/images/service/logo.png]";
            let result = imageProcessor.toHTML(text);
            expect(result).toBe('Image: <img src="http://kozalo.ru/images/service/logo.png" alt="Logo">');
        });

        it("Some images", function() {
            let text = "The first image: ![[https://pp.vk.me/c633826/v633826631/13820/hdP8yOQyksg.jpg]]. And the second one: ![GreenTeaNeko][https://pp.vk.me/c633826/v633826631/13817/bwibT9eHELI.jpg]";
            let result = imageProcessor.toHTML(text);
            expect(result).toBe('The first image: <img src="https://pp.vk.me/c633826/v633826631/13820/hdP8yOQyksg.jpg">. And the second one: <img src="https://pp.vk.me/c633826/v633826631/13817/bwibT9eHELI.jpg" alt="GreenTeaNeko">');
        });

        it("Images and links", function() {
            let text = "@[Website][http://kozalo.ru]: @[[http://kozalo.ru]]. ![[http://kozalo.ru/images/service/logo.png]]![Logo][http://kozalo.ru/images/service/logo.png]";
            let result = imageProcessor.toHTML(text);
            expect(result).toBe('<a href="http://kozalo.ru">Website</a>: <a href="http://kozalo.ru">http://kozalo.ru</a>. <img src="http://kozalo.ru/images/service/logo.png"><img src="http://kozalo.ru/images/service/logo.png" alt="Logo">');
        });
    });


    describe("Tests for transformLists()", function() {
        describe("Without indents", function() {
            let listsOnlyProcessor = new KozMUL({
                transformHTMLEntities: false,
                transformBasicStyleTags: false,
                transformTypographicalCharacters: false,
                transformSubscriptAndSuperscriptTags: false,
                transformParagraphs: false,
                transformLists: true,
                transformQuotations: false,
                transformHorizontalLines: false,
                transformHeadings: false,
                transformLinks: false,
                transformImages: false,
                transformTables: false,
                transformCodeTags: false,
                transformSubstitutions: false
            }, {
                listsWithIndentations: false
            });

            it("Bulleted lists", function() {
                let text1 = "+++ First;[[eol]]+ Second;[[eol]]# Third.";
                let text2 = "+ First;[[eol]]+ Second;[[eol]]# Third.";
                let result1 = listsOnlyProcessor.toHTML(text1);
                let result2 = listsOnlyProcessor.toHTML(text2);
                expect(result1).toBe("<ul><li>First;</li><li>Second;</li><li>Third.</li></ul>");
                expect(result2).toBe("<ul><li>First;</li><li>Second;</li><li>Third.</li></ul>");
            });

            it("Numbered lists", function() {
                let text1 = "### First;[[eol]]# Second;[[eol]]+ Third.";
                let text2 = "# First;[[eol]]# Second;[[eol]]+ Third.";
                let result1 = listsOnlyProcessor.toHTML(text1);
                let result2 = listsOnlyProcessor.toHTML(text2);
                expect(result1).toBe("<ol><li>First;</li><li>Second;</li><li>Third.</li></ol>");
                expect(result2).toBe("<ol><li>First;</li><li>Second;</li><li>Third.</li></ol>");
            });

            it("Mixed lists", function() {
                let text = "### First;[[eol]]# Second;[[eol]]## Third.[[eol]]Outer text.[[eol]]+++ First;[[eol]]+ Second;[[eol]]+ Third.[[eol]]+[[eol]]Another piece of text.";
                let result = listsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ol><li>First;</li><li>Second;</li><li>Third.</li></ol>Outer text.<br/><ul><li>First;</li><li>Second;</li><li>Third.</li></ul>Another piece of text.");
            });

            it("Nested lists", function() {
                let text = "### One;[[eol]]# Two;[[eol]]# Three.[[eol]]### Nested one;[[eol]]# Nested two;[[eol]]## Nested three.[[eol]]## Four.[[eol]]+++ One;[[eol]]+ Two;[[eol]]+ Three.[[eol]]### Nested one;[[eol]]# Nested two;[[eol]]#[[eol]]+ Four.";
                let result = listsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ol><li>One;</li><li>Two;</li><li>Three.</li><ol><li>Nested one;</li><li>Nested two;</li><li>Nested three.</li></ol><li>Four.</li></ol><ul><li>One;</li><li>Two;</li><li>Three.</li><ol><li>Nested one;</li><li>Nested two;</li></ol><li>Four.</li></ul>");
            });
        });
        
        describe("With indents", function() {
            let indentListsOnlyProcessor = new KozMUL({
                transformHTMLEntities: false,
                transformBasicStyleTags: false,
                transformTypographicalCharacters: false,
                transformSubscriptAndSuperscriptTags: false,
                transformParagraphs: false,
                transformLists: true,
                transformQuotations: false,
                transformHorizontalLines: false,
                transformHeadings: false,
                transformLinks: false,
                transformImages: false,
                transformTables: false,
                transformCodeTags: false,
                transformSubstitutions: false
            }, {
                listsWithIndentations: true
            });

            it("Bulleted lists", function() {
                let text = "+ First;[[eol]]+ Second;[[eol]]# Third.";
                let result = indentListsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ul><li>First;</li><li>Second;</li><li>Third.</li></ul>");
            });

            it("Numbered lists", function() {
                let text = "# First;[[eol]]# Second;[[eol]]+ Third.";
                let result = indentListsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ol><li>First;</li><li>Second;</li><li>Third.</li></ol>");
            });

            it("Mixed lists", function() {
                let text = "# First;[[eol]]# Second;[[eol]]# Third.[[eol]]#[[eol]]Outer text.[[eol]]+ First;[[eol]]+ Second;[[eol]]+ Third.[[eol]]+[[eol]]Another piece of text.";
                let result = indentListsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ol><li>First;</li><li>Second;</li><li>Third.</li></ol>Outer text.<br/><ul><li>First;</li><li>Second;</li><li>Third.</li></ul>Another piece of text.");
            });

            it("Nested lists", function() {
                let text = "# One;[[eol]]# Two;[[eol]]# Three.[[eol]] # Nested one;[[eol]] # Nested two;[[eol]] # Nested three.[[eol]]# Four.[[eol]]#[[eol]]+ One;[[eol]]+ Two;[[eol]]+ Three.[[eol]] # Nested one;[[eol]] # Nested two;[[eol]]+ Four.";
                let result = indentListsOnlyProcessor.toHTML(text);
                expect(result).toBe("<ol><li>One;</li><li>Two;</li><li>Three.</li><ol><li>Nested one;</li><li>Nested two;</li><li>Nested three.</li></ol><li>Four.</li></ol><ul><li>One;</li><li>Two;</li><li>Three.</li><ol><li>Nested one;</li><li>Nested two;</li></ol><li>Four.</li></ul>");
            });
        });
    });


    describe("Tests for transformQuotations()", function() {
        let quotationProcessor = new KozMUL({
            transformHTMLEntities: false,
            transformBasicStyleTags: false,
            transformTypographicalCharacters: false,
            transformSubscriptAndSuperscriptTags: false,
            transformParagraphs: true,
            transformLists: false,
            transformQuotations: true,
            transformHorizontalLines: false,
            transformHeadings: false,
            transformLinks: false,
            transformImages: false,
            transformTables: false,
            transformCodeTags: false,
            transformSubstitutions: false
        });

        it("Single quote", function() {
            let text = "&gt; Quote";
            let result = quotationProcessor.toHTML(text);
            expect(result).toBe('<blockquote><p>Quote</p></blockquote>');
        });

        it("Inside text", function() {
            let text = "Blah-blah-blah[[eol]]&gt; Quote[[eol]]blah-blah-blah";
            let result = quotationProcessor.toHTML(text);
            expect(result).toBe('<p>Blah-blah-blah</p><blockquote><p>Quote</p></blockquote><p>blah-blah-blah</p>');
        });

        it("Two paragraphs", function() {
            let text = "Blah-blah-blah[[eol]]&gt; Paragraph 1[[eol]]&gt;[[eol]]&gt; Paragraph 2[[eol]]blah-blah-blah";
            let result = quotationProcessor.toHTML(text);
            expect(result).toBe('<p>Blah-blah-blah</p><blockquote><p>Paragraph 1</p><p>Paragraph 2</p></blockquote><p>blah-blah-blah</p>');
        });

        it("Each line is wrapped with paragraph", function() {
            let text = "Blah-blah-blah[[eol]][[eol]]&gt; Paragraph 1[[eol]]&gt;[[eol]]&gt; Paragraph 2[[eol]][[eol]]blah-blah-blah";
            let result = quotationProcessor.toHTML(text);
            expect(result).toBe('<p>Blah-blah-blah</p><blockquote><p>Paragraph 1</p><p>Paragraph 2</p></blockquote><p>blah-blah-blah</p>');
        });
    });


    describe("Tests for transformTables()", function() {
        let tableProcessor = new KozMUL({
            transformHTMLEntities: false,
            transformBasicStyleTags: false,
            transformTypographicalCharacters: false,
            transformSubscriptAndSuperscriptTags: false,
            transformParagraphs: false,
            transformLists: false,
            transformQuotations: false,
            transformHorizontalLines: false,
            transformHeadings: false,
            transformLinks: false,
            transformImages: false,
            transformTables: true,
            transformCodeTags: false,
            transformSubstitutions: false
        }, {
            tableClasses: ['table', 'table-bordered']
        });

        it("One cell", function() {
            let text = "[table][row][cell]Data[/cell][/row][/table]";
            let result = tableProcessor.toHTML(text);
            expect(result).toBe('<table class="table table-bordered"><tr><td>Data</td></tr></table>');
        });

        it("One row two cells", function() {
            let text = "[table][row][cell]One[/cell][cell]Two[/cell][/row][/table]";
            let result = tableProcessor.toHTML(text);
            expect(result).toBe('<table class="table table-bordered"><tr><td>One</td><td>Two</td></tr></table>');
        });

        it("Two rows three cells", function() {
            let text = "[table][row][cell]One[/cell][cell]Two[/cell][/row][row][cell]Three[/cell][/row][/table]";
            let result = tableProcessor.toHTML(text);
            expect(result).toBe('<table class="table table-bordered"><tr><td>One</td><td>Two</td></tr><tr><td>Three</td></tr></table>');
        });

        it("Without classes", function() {
            let classlessProcessor = new KozMUL({
                transformHTMLEntities: false,
                transformBasicStyleTags: false,
                transformTypographicalCharacters: false,
                transformSubscriptAndSuperscriptTags: false,
                transformParagraphs: false,
                transformLists: false,
                transformQuotations: false,
                transformHorizontalLines: false,
                transformHeadings: false,
                transformLinks: false,
                transformImages: false,
                transformTables: true,
                transformCodeTags: false,
                transformSubstitutions: false
            }, {
                tableClasses: []
            });

            let text = "[table][row][cell]One[/cell][cell]Two[/cell][/row][row][cell]Three[/cell][/row][/table]";
            let result = classlessProcessor.toHTML(text);
            expect(result).toBe('<table><tr><td>One</td><td>Two</td></tr><tr><td>Three</td></tr></table>');
        });
    });


    describe("Tests for transformSubstitutions()", function() {
        it("Only substitutions under testing", function() {
            let text = "{{rf=Russian Federation}}I'm from {{rf}}.";
            let result = processor._transformSubstitutions(text);
            expect(result).toBe("I'm from Russian Federation.");
        });

        it("Substitutions + basic transformations", function() {
            let text = "blah-blah       {{one=1}} blah-blah {{two=2}} blah-blah blah-blah number {{one}} blah-blah the second {{two}} blah-blah";
            let result = processor._basicTransformations(processor._transformSubstitutions(text));
            expect(result).toBe('blah-blah        blah-blah blah-blah blah-blah number 1 blah-blah the second 2 blah-blah');
        });
    });


    describe("Everything is under the test", function() {
        it("Complex test", function() {
            let text = "== **Hello^{everybody}!**\n\n<strong>Here</strong> is\nan ~~__italic__ test~~ line  *\\\\*break*\\\\*.\n\nIt consists of two <<para--\n\nHere is a list:\n+ One;\n+ Two;\n++ Three.\nAnother one:\n# 1\n# 2\n+++ 2.1\n+ 2.2\n+\n## 3\n\n------\n\n> > @[My website][http://kozalo.ru]: @[[http://kozalo.ru]]\n> @![[http://kozalo.ru/images/service/logo.png]]![Logo][http://kozalo.ru/images/service/logo.png]";
            let result = processor.toHTML(text);
            expect(result).toBe('<h3><strong>Hello<sup>everybody</sup>!</strong></h3><p>&lt;strong&gt;Here&lt;/strong&gt; is<br/>an <s><em>italic</em> test</s> line **break**.</p><p>It consists of two &laquo;para&mdash;</p><p>Here is a list:</p><ul><li><p>One;</p></li><li><p>Two;</p></li><li><p>Three.</p></li></ul><p>Another one:</p><ol><li><p>1</p></li><li><p>2</p></li><ul><li><p>2.1</p></li><li><p>2.2</p></li></ul><li><p>3</p></li></ol><p><hr/></p><blockquote><p>&gt; <a href="http://kozalo.ru">My website</a>: <a href="http://kozalo.ru">http://kozalo.ru</a><br/><a href="http://kozalo.ru/images/service/logo.png"><img src="http://kozalo.ru/images/service/logo.png"></a><img src="http://kozalo.ru/images/service/logo.png" alt="Logo"></p></blockquote>');
        });

        it("With default parameters", function() {
            let defaultProcessor = new KozMUL();
            let text = "== **Hello^{everybody}!**\n\n<strong>Here</strong> is\nan ~~__italic__ test~~ line  *\\\\*break*\\\\*.\n\nIt consists of two <<para--\n\nHere is a list:\n+ One;\n+ Two;\n+ Three.\n+\nAnother one:\n# 1\n# 2\n + 2.1\n + 2.2\n# 3\n#\n\n------\n\n> > @[My website][http://kozalo.ru]: @[[http://kozalo.ru]]\n> @![[http://kozalo.ru/images/service/logo.png]]![Logo][http://kozalo.ru/images/service/logo.png]";
            let result = defaultProcessor.toHTML(text);
            expect(result).toBe('<p>== <strong>Hello<sup>everybody</sup>!</strong></p><p>&lt;strong&gt;Here&lt;/strong&gt; is<br/>an <s><em>italic</em> test</s> line **break**.</p><p>It consists of two &laquo;para&mdash;</p><p>Here is a list:</p><ul><li><p>One;</p></li><li><p>Two;</p></li><li><p>Three.</p></li></ul><p>Another one:</p><ol><li><p>1</p></li><li><p>2</p></li><ul><li><p>2.1</p></li><li><p>2.2</p></li></ul><li><p>3</p></li></ol><p>&mdash;&mdash;&mdash;</p><blockquote><p>&gt; @[My website][http://kozalo.ru]: @[[http://kozalo.ru]]<br/>@![[http://kozalo.ru/images/service/logo.png]]![Logo][http://kozalo.ru/images/service/logo.png]</p></blockquote>');
        })
    });
});
