KozMUL
========

Kozalo's Mark-Up Language
--------------------------------------------------

Repository: https://Kozalo@bitbucket.org/Kozalo/kozmul.git  
Bug tracker: https://bitbucket.org/Kozalo/kozmul/issues  

### Для русскоязычных людей

Подробное руководство на русском языке можно прочитать на [моём сайте](http://nekochan.ru/KozMUL/manual/ru/).  

### Introduction

Hello, colleague! Let me introduce you my own mark-up language -- KozMUL! It's similar to Markdown but has more features, and its syntax is a bit prettier (for me, of course ;) ).  
In this short tutorial, I'd like to consider all main features of KozMUL as quickly as possible.  
Actually, the library mostly is just a set of several regular expressions and some more complex (and, probably, stupid) functions.

### How to use the library

1. Firstly, you must include KozMUL.js and the set of libraries named KozUtils to your HTML file.  
```
<script src="http://libs.nekochan.ru/KozUtils-AIO.js"></script>  
<script src="http://libs.nekochan.ru/KozMUL.js"></script>
```  
2. KozMUL.js has only one class: KozMUL. Let's create an instance of it!  
```
let textProcessor = new KozMUL({  
    transformHTMLEntities: true,  
    transformBasicStyleTags: true,  
    transformTypographicalCharacters: true,  
    transformSubscriptAndSuperscriptTags: true,  
    transformParagraphs: true,  
    transformLists: true,  
    transformQuotations: true,  
    transformHorizontalLines: false,  
    transformHeadings: false,  
    transformLinks: false,  
    transformImages: false,  
    transformTables: false,  
    transformCodeTags: false,  
    transformSubstitutions: false  
}, {  
    targetBlank: false,  
    tableClasses: ['table', 'table-bordered'],  
    listsWithIndentations: true  
});
```  
In the example above all possible options are demonstrated. Both objects are optional. If you call the constructor without any parameters, you get exactly the same instance. So, you're in charge to reassign any parameters against their default values.  
OK, the first object in the argument list is called *operations* and consists of all transformations that will be applied to text. The second one is called *options* and contains parameters for some of operations. We're talk about it more later on.  
3. The last thing we need to do to covert out KozMUL *text* to HTML code is call ``textProcessor.toHTML(text)``. That's all! Pretty simple, isn't it?

### And what about the language itself?

Oh, it's a good time to speak about <s>God</s> the syntax of KozMUL!  
Look at the list below! I'll trying to explain you the basics of the usage of KozMUL. In parentheses at the beginning of every string, corresponding *operation* without "transform" prefix is written.

* (BasicStyleTags) You can use \*\*__bold__\*\*, \_\_*italic*\_\_, ~~<s>strikethrough</s>~~ text.
* (TypographicalCharacters) KozMUL has support for some typographical characters: — (``--``), «» (``<<>>``), © (``(c)``), ® (``(r)``), ™ (``(tm)``).
* (HTMLEntities) All HTML tags, quotes and ampersands will be escaped automatically: ``<b> => &lt;b&gt;``, ``" => &quot;``, ``& => &amp;``.
* (SubscriptAndSuperscriptTags) To write a formula, use this syntax: ``x.{1}=2^{2}=4`` => x<sub>1</sub>=2<sup>2</sup>=4
* (Links) To create a link you should write something like these two: ``@[[http://kozalo.ru]]`` => http://kozalo.ru, ``@[Meow!][http://nekochan.ru]`` => [Meow!](http://nekochan.ru)  
Note that there is the option called *targetBlank*. If it's set, all links will be processed with the ``target=_blank`` attribute.
* (Images) Syntax for images is very similar (I won't give you a working example, okay?): ``![[http://nekochan.ru/images/head_of_nekochan.png]]``, ``![Logo][http://kozalo.ru/images/service/logo.png]``, or even ``@![[http://nekochan.ru/images/im_nekochan_vaper.jpg]]``.  
The reader can ask me: "What's the difference between them?" And I'll answer! The first example will be processed as just an image, the second one, on contrary, will be an image with the *alt* tag, and, finally, the last will be an image wrapped with a link to it!
* (Quotations) ``> Liberty or Death``. Yes, just like in Markdown.
* (CodeTags) Like in Markdown again. Two back quotes (`) for wrapping with the ``<code>`` tag and three back quotes for the ``<pre>`` tag.
* (HorizontalLines) How can you write ``<hr>``? It's very easy! Just write 6 hyphens (-) / 3 dashes or more in a line without any other symbols (yeah, Markdown again and again).
* (Substitutions) I think the reader is smart enough to realize how to use substitutions in KozMUL using only an example, huh?  
``{{s=ubstitution}} S{{s}}, s{{s}}, s{{s}}s...`` => ``Substitution, substitution, substitutions...``
* (Paragraphs) If the operation is enabled, the library tries to divide text to paragraphs. I spent a long time in attempts to make it work correctly in most cases, and I hope it actually works properly. But I cannot always guarantee you valid code. Sorry. :(  If you have enough experience and desire to improve my simple dumb algorithms, welcome!

OK, guys, we're almost done. We have just 4 features left to cover the whole library. I want to tell about them separately.

#### Headings

If Markdown supports 6 headings, KozMUL supports only 3. By default they are ``<h3>``, ``<h4>`` and ``<h5>``. I urge you to use KozMUL as an additional language, not for a whole HTML page. So, ``<h1>`` and ``<h2>`` should be set by usual HTML code.  
But actually you can use the *mostImportantHeading* parameter of the constructor to change the reference point and start with, for example, ```<h2>```.  

```
== Heading 1 ==
  
  
$$ Heading 2
  
Lorem ipsum...
```

To use ``<h3>``, you must type two equal signs (==), title, and equal signs again (optional). To use ``<h4>``, you must type two dollar signs ($$), title, and dollar signs again (optional).  To use ``<h5>``, use two percent signs (%%).  
Also, don't forget about blank lines before and after a heading!  

#### Lists

It's one of the most complex part of the library and the language. Let's start with symbols. I don't like the way which Markdown uses. Therefore, I use a plus sign (+) for unordered lists and a number sign (#) for ordered lists. They just determine the type of a list. In usage they both are equal.  
OK, we're done here. Let's move forward! There are two variants of syntax for lists:
1. The first one (it's used by default) uses indentations. Look at the example:
```
# First line.
# Second line.
 + 2.1;
 + 2.2;
 + 2.3;
# Third line.
#
```
Pretty simple, huh? I think only the last line needs an explanation. Since items of a list can consists of many lines, the library needs some help to determine the end of the list. The blank item at the end of the list plays this role! Strictly speaking, there is a way to get rid of it. Just add indentations to all lines again!  
```
 # First line.
 # Second line.
  + 2.1;
  + 2.2;
  + 2.3;
 # Third line.
```
In this case, the parser will be able to determine the end of the list by itself without any additional help!  
2. The second variant to create a list uses combinations of pluses or number signs. Here's the example:  
```
# First line.  
# Second line.
+++ 2.1;
+ 2.2;
++ 2.3;
# Third line.
#
```
Brr! Looks ugly, doesn't it? Except one sign, we can use:
    1. Two pluses or number signs (++ or ##) to close the list. The line 2.3 closes a whole sublist using this syntax. But the way with a blank item works here too.
    2. Three pluses or number signs (+++ or ###) to open a new list or sublist. Due to its verbosity, usually this syntax is used only for sublists (the library can detect the beginning of a list by only one sign).
I think this syntax can be useful when you have lots of sublists with many items. In this case the necessity of indentations will be pretty tedious.  

We went through it!

#### Tables

Currently, the library just translates ``[table]``, ``[row]``, ``[cell]``, and ``[header]`` tags to their HTML equivalents: ``<table>``, ``<tr>``, ``<td>``, and ``<th>``. Thus, you have to write a lot of reduntant code. Let's look at this complex example:  

```
[table]
    [row]
        [cell]dfdfdf[/cell]
        [cell]fdfdfdfdfd[/cell]
        [cell]
            [table]
                [row]
                    [cell]dfdffd[/cell]
                [/row]
                [row]
                    [cell]fdfdfd[/cell]
                [/row]
            [/table]
        [/cell]
    [/row]
    [row]
        [cell]dfdfdf[/cell]
        [cell]fdfdfdfdfd[/cell]
    [/row]
[/table]
```

FUUUUUUUUUUUUU!!1  
What if I tell you that somewhen we'll be able to rewrite it to following:  

```
[row]  
    [cell]dfdfdf[/cell]  
    [cell]fdfdfdfdfd[/cell]  
    [cell]
        [row]dfdffd[/row]
        [row]fdfdfd[/row]
    [/cell]
[/row]
[row]
    [cell]dfdfdf[/cell]
    [cell]fdfdfdfdfd[/cell]
[/row]
```

Better? Much better!  
I'm going to work on implementation a new table parser. It will take texts with simplified tables, build trees, repaire the structure of tables, and return the correct HTML tables. But it's a question of time.  
Currently, table syntax in KozMUL is pretty simple. Even primitive, I would say.  

### Conclusion

OK, I think we've covered all aspects of KozMUL and reached the end of the manual. I hope you'll find this library as useful as I do.  
Also, I'm always glad if someone wants to contribute a piece of code to make the library better. If you want to donate some money instead, let me know via [email](mailto:kozalo@yandex.ru) or [my website](http://kozalo.ru).
